# Projected Roadmap

- Phishme / Gophish integration (to launch targeted phishing campaings against current
  registered domains)
- Route53 integration (to import custom domains from the DNS service)
- Office365 integration (in addition to Google)
- Make the authentication Google-agnostic
- Mobile support
- Safari Semaforo Extension
- Haveibeenpwned integration
