# Semaforo Project

The roadmap and general documentation container repository for the Semaforo project.
Please see `ROADMAP.md` for the last roadmap.


## Source Code

Source code is available on the following repos:


- https://gitlab.com/clovrlabs/semaforo-browser-extension
- https://gitlab.com/clovrlabs/semaforo-backend
- https://gitlab.com/clovrlabs/semaforo-frontend

### contacts

https://t.me/semaforoapp
https://semaforo.tech
